![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/NoahJelen/nnparadisemod/1.12.2)
![For Minecraft 1.12.2](https://img.shields.io/badge/Minecraft-1.12.2-green)
# Nether Noah's Paradise Mod
![](src/main/resources/assets/nnparadisemod/textures/nnparadisemod.png)

The Paradise Mod is a mod for Minecraft 1.12.2 that adds various features to the game to improve it. Among these features are new biomes, better looking caves, more variants of buttons and pressure plates, and 2 new dimensions.

[website](https://www.paradisemod.net/)
