package net.paradisemod.decoration;

import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.world.blocks.CustomEndPlant;
import net.paradisemod.base.blocks.CustomPlant;
import net.paradisemod.decoration.blocks.CactusBookshelf;
import net.paradisemod.decoration.GUI.CustomCraftBenchGuiHandler;
import net.paradisemod.decoration.blocks.CustomGlass;
import net.paradisemod.decoration.blocks.CustomPane;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.Level;

public class Decoration {
    // metal bars
    public static CustomPane GoldBars = new CustomPane(Material.IRON, true, SoundType.METAL, 10F,"gold_bars","GoldBars");
    public static CustomPane RustyIronBars = new CustomPane(Material.IRON, true, SoundType.METAL, 10F,"rusty_iron_bars","RustyIronBars");
    public static CustomPane SilverBars = new CustomPane(Material.IRON, true, SoundType.METAL, 10F,"silver_bars","SilverBars");

    // glass
    public static CustomGlass bulletproofGlass = new CustomGlass("bulletproof_glass","bulletProofGlass");
    public static CustomPane bulletproofGlassPane = new CustomPane(Material.GLASS, true, SoundType.GLASS,2000F,"bulletproof_glass_pane","bulletproofGlassPane");
    public static CustomGlass soulGlass = new CustomGlass("soul_glass","soulGlass");
    public static CustomPane soulGlassPane = new CustomPane(Material.GLASS, false, SoundType.GLASS, 0.5F,"soul_glass_pane","soulGlassPane");

    // flowers
    public static CustomPlant BlueRose = new CustomPlant(false);
    public static CustomPlant Rose = new CustomPlant(false);
    public static CustomEndPlant EnderRose = new CustomEndPlant(false);

    // others
    public static Block CactusBookshelf = new CactusBookshelf();
    public static Block Stonecutter = new Block(Material.ROCK);

    public static void init() {
        //metal bars
        GoldBars.setHarvestLevel("pickaxe", 2);
        RustyIronBars.setHarvestLevel("pickaxe", 0);
        SilverBars.setHarvestLevel("pickaxe", 2);

        Utils.regBlock(GoldBars.setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.DECORATIONS));
        Utils.regBlock(RustyIronBars.setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.DECORATIONS));
        Utils.regBlock(SilverBars.setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.DECORATIONS));

        //glass
        soulGlassPane.setDefaultSlipperiness(1.5F);
        soulGlass.setDefaultSlipperiness(1.2F);

        Utils.regBlock(soulGlassPane.setHardness(.5F).setResistance(1F).setLightLevel(1F));
        Utils.regBlock(soulGlass.setHardness(.5F).setResistance(1F).setLightLevel(1F));
        Utils.regBlock(bulletproofGlass.setHardness(.5F).setResistance(2000F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        Utils.regBlock(bulletproofGlassPane.setHardness(.5F).setResistance(2000F).setCreativeTab(CreativeTabs.DECORATIONS));

        //flowers
        Utils.regBlock(BlueRose.setUnlocalizedName("BlueRose").setRegistryName("blue_rose"));
        Utils.regBlock(Rose.setUnlocalizedName("Rose").setRegistryName("rose"));
        Utils.regBlock(EnderRose.setUnlocalizedName("EnderRose").setRegistryName("ender_rose"));

        Utils.regBlock(CactusBookshelf);
        Utils.regBlock(Stonecutter.setUnlocalizedName("Stonecutter").setRegistryName("stonecutter").setHardness(5F).setResistance(5.8333333333F).setCreativeTab(CreativeTabs.DECORATIONS));

        // smelting recipes
        GameRegistry.addSmelting(Blocks.SOUL_SAND, new ItemStack(soulGlass), .01F);

        Tables.init();

        ParadiseMod.LOG.log(Level.INFO,"Loaded decoration module");
    }

    public static  void initTableGUI() {
        NetworkRegistry.INSTANCE.registerGuiHandler(ParadiseMod.instance, new CustomCraftBenchGuiHandler());
    }

    public static void regRenders() {
        Utils.regRender(GoldBars);
        Utils.regRender(RustyIronBars);
        Utils.regRender(SilverBars);
        Utils.regRender(bulletproofGlass);
        Utils.regRender(bulletproofGlassPane);
        Utils.regRender(soulGlass);
        Utils.regRender(soulGlassPane);
        Utils.regRender(EnderRose);
        Utils.regRender(BlueRose);
        Utils.regRender(Rose);
        Utils.regRender(CactusBookshelf);
        Utils.regRender(Stonecutter);

        Tables.regRenders();
    }
}
