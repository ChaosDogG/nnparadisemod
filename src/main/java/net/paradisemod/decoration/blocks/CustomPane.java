package net.paradisemod.decoration.blocks;

import net.minecraft.block.BlockPane;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.BlockRenderLayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

//base class for custom glass panes
public class CustomPane extends BlockPane {
	private final Material blockMaterial;
	public CustomPane(Material material, boolean isMetal, SoundType sound, float resistance,String regName, String ULName) {
		super(material, isMetal);
		blockMaterial=material;
		setSoundType(sound);
		setResistance(resistance);
		setRegistryName(regName);
		setUnlocalizedName(ULName);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getBlockLayer() {
		if (blockMaterial!=Material.GLASS)
			return BlockRenderLayer.SOLID;
		else
			return BlockRenderLayer.TRANSLUCENT;
	}
}