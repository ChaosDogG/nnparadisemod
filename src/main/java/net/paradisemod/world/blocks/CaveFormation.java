package net.paradisemod.world.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Random;

import static net.minecraft.util.EnumFacing.UP;

// base class for the cave formation blocks
public class CaveFormation extends BlockDirectional {
    protected static final AxisAlignedBB FORMATION_VERTICAL = new AxisAlignedBB(0.3125D, 0.0D, 0.3125D, 0.6875D, 1.0D, 0.6875D);
    protected static final AxisAlignedBB FORMATION_NS = new AxisAlignedBB(0.3125D, 0.3125D, 0.0D, 0.6875D, 0.6875D, 1.0D);
    protected static final AxisAlignedBB FORMATION_EW = new AxisAlignedBB(0.0D, 0.3125D, 0.3125D, 1.0D, 0.6875D, 0.6875D);

    public CaveFormation(SoundType sound)
    {
        super(Material.CIRCUITS);
        setDefaultState(this.blockState.getBaseState().withProperty(FACING, UP));
        setCreativeTab(CreativeTabs.DECORATIONS);
        setSoundType(sound);
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side) {
        return isSupport(world, side, pos);
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block block, BlockPos fromPos)
    {
        if (!isSupport(world, state.getValue(FACING), pos)) {
            world.setBlockState(pos, Blocks.AIR.getDefaultState());
        }
    }

    private boolean isSupport(World world, EnumFacing direction, BlockPos pos) {
        BlockPos support_pos = pos.down();
        switch (direction) {
            case UP:
                support_pos = pos.down();
                break;

            case DOWN:
                support_pos = pos.up();
                break;

            case NORTH:
                support_pos = pos.south();
                break;

            case SOUTH:
                support_pos = pos.north();
                break;

            case EAST:
                support_pos = pos.west();
                break;

            case WEST:
                support_pos = pos.east();
                break;
        }

        return world.getBlockState(support_pos).isTopSolid();
    }

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getBlockLayer() {
		return BlockRenderLayer.TRANSLUCENT;
	}
    @Override
	protected boolean canSilkHarvest() {
		return true;
	}
    @Override
    public int quantityDropped(Random random)
    {
        return 0;
    }
    @Override
    public boolean isPassable(IBlockAccess worldIn, BlockPos pos)
    {
        return true;
    }

    @Override
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos)
    {
        return true;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        switch (state.getValue(FACING).getAxis())
        {

            case X:
            default:
                return FORMATION_EW;
            case Z:
                return FORMATION_NS;
            case Y:
                return FORMATION_VERTICAL;
        }
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState();
        iblockstate = iblockstate.withProperty(FACING, EnumFacing.getFront(meta));
        return iblockstate;
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        return ((EnumFacing)state.getValue(FACING)).getIndex();
    }
    
    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        IBlockState iblockstate = worldIn.getBlockState(pos.offset(facing.getOpposite()));

        if (iblockstate.getBlock() instanceof CaveFormation)
        {
            EnumFacing enumfacing = iblockstate.getValue(FACING);

            if (enumfacing == facing)
                return this.getDefaultState().withProperty(FACING, facing.getOpposite());
        }

        return this.getDefaultState().withProperty(FACING, facing);
    }
    
    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {FACING});
    }
}