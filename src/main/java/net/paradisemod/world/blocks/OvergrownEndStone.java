package net.paradisemod.world.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;

import java.util.Random;
//class for the Overgrown End Stone block
public class OvergrownEndStone extends Block implements IGrowable{
	public OvergrownEndStone() {
		super(Material.WOOD);
		setUnlocalizedName("OverGrownEndStone");
		setRegistryName("overgrown_end_stone");
		setHardness(5F);
		setResistance(15F);
		setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		setSoundType(SoundType.STONE);
		setTickRandomly(true);
	}

	@Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Blocks.END_STONE.getItemDropped(Blocks.END_STONE.getDefaultState(), rand, fortune);
    }

	@Override
    public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient)
    {
        return true;
    }

	@Override
	public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state) {
		return true;
	}

	@Override
	public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state) {
	}

	@Override
	public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
	{
		if (!world.isAreaLoaded(pos, 3)) return;

		BlockPos[] adjBlocks = {
			pos.west(),pos.east(),pos.north(),pos.south(),
			pos.up().west(),pos.up().east(),pos.up().north(),pos.up().south(),
			pos.down().west(),pos.down().east(),pos.down().north(),pos.down().south(),
		};

		BlockPos blockToSpreadto = adjBlocks[rand.nextInt(12)];

		Block adjBlock = world.getBlockState(blockToSpreadto).getBlock();
		IBlockState blockAbove = world.getBlockState(blockToSpreadto.up());

		if (world.getBlockState(pos.up()).isFullBlock() || world.getBlockState(pos.up()).getBlock() instanceof BlockLiquid || world.getBlockState(pos.up()).getBlock() instanceof BlockFluidClassic) world.setBlockState(pos, Blocks.END_STONE.getDefaultState());

		if (adjBlock == Blocks.END_STONE && !blockAbove.isFullBlock() && !(blockAbove.getBlock() instanceof BlockLiquid) && !(blockAbove.getBlock() instanceof BlockFluidClassic)) world.setBlockState(blockToSpreadto, getDefaultState());
	}
}