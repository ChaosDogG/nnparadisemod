package net.paradisemod.world.gen.structures;

import java.util.Random;

import net.paradisemod.base.ModConfig;

import net.paradisemod.world.modWorld;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class RoguePortal implements IWorldGenerator{
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.RoguePortal==false)
			return;
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;
		if (world.provider.getDimension() == 0){
			int y = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ, modWorld.ground);
			if (y<=31)
				return;
			BlockPos pos = new BlockPos(blockX, y, blockZ);
			
			if(rand.nextInt(ModConfig.worldgen.structures.RoguePortalChance) == 0){
				boolean isZ = rand.nextBoolean();
				for (int a=0;a<=3;a++) {
					for (int by = 0; by <= 4; by++) {
						if (isZ)
							world.setBlockState(pos.add(0,by,a), Blocks.OBSIDIAN.getDefaultState());
						else
							world.setBlockState(pos.add(a,by,0), Blocks.OBSIDIAN.getDefaultState());
					}
				}

				for (int a=1;a<=2;a++) {
					for (int by = 1; by <= 3; by++) {
						if (isZ)
							world.setBlockState(pos.add(0,by,a), Blocks.FIRE.getDefaultState(),3);
						else
							world.setBlockState(pos.add(a,by,0), Blocks.FIRE.getDefaultState(),3);
					}
				}
			}
		}
	}
}