package net.paradisemod.world.gen.structures;

import java.util.Random;

import net.paradisemod.base.ModConfig;

import net.paradisemod.redstone.Plates;
import net.paradisemod.world.modWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStone;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class LandMine implements IWorldGenerator{
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.Landmines==false)
			return;
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;
		int blockY = modWorld.getGroundFromAbove(world, 10, 255, blockX, blockZ, modWorld.ground);
		BlockPos pos = new BlockPos(blockX,blockY,blockZ);

		if(rand.nextInt(ModConfig.worldgen.structures.LandminesChance) == 0){
			Block ground = world.getBlockState(pos).getBlock();
			BlockStone.EnumType stoneType = null;
			if (ground==Blocks.STONE)
				stoneType = world.getBlockState(pos).getValue(BlockStone.VARIANT);
			Block ground2 = world.getBlockState(pos).getBlock();

			if (blockY<=10)
				return;

			for (int x=-1;x<=1;x++){
				for (int z=-1;z<=1;z++){
					ground2 = world.getBlockState(pos.add(x,0,z)).getBlock();
					if (ground2==Blocks.AIR)
						return;
				}
			}

			if (ground==Blocks.GRASS)
				world.setBlockState(pos.up(), Plates.GrassPlate.getDefaultState());
			if (ground==Blocks.DIRT)
				world.setBlockState(pos.up(), Plates.DirtPlate.getDefaultState());
			if (ground==Blocks.STONE) {
				world.setBlockState(pos.up(), Blocks.STONE_PRESSURE_PLATE.getDefaultState());
				if (stoneType == BlockStone.EnumType.ANDESITE)
					world.setBlockState(pos.up(), Plates.AndesitePlate.getDefaultState());
				if (stoneType == BlockStone.EnumType.DIORITE)
					world.setBlockState(pos.up(), Plates.DioritePlate.getDefaultState());
				if (stoneType == BlockStone.EnumType.GRANITE)
					world.setBlockState(pos.up(), Plates.GranitePlate.getDefaultState());
			}

			for (int x=-1;x<=1;x++){
				for (int z=-1;z<=1;z++)
					world.setBlockState(pos.add(x,-1,z),Blocks.TNT.getDefaultState());
			}
		}
	}
}