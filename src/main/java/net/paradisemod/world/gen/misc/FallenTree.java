package net.paradisemod.world.gen.misc;

import net.paradisemod.world.modWorld;
import net.paradisemod.world.biome.BiomeSaltFlat;
import net.paradisemod.world.biome.BiomeTemperateJungle;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockVine;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.biome.*;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class FallenTree implements IWorldGenerator {
    //valid log types
    public static IBlockState[] logs = {
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH),
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE),
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE),
            Blocks.LOG.getDefaultState()
    };

    @Override
    public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        int blockX = chunkX*16;
        int blockZ = chunkZ*16;
        int blockY = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ, modWorld.ground);

        //position of the stump
        BlockPos pos = new BlockPos(blockX,blockY,blockZ);

        //should it generate?
        if (rand.nextInt(1)!=0)
            return;

        //length of the log
        int length = 4+rand.nextInt(3);

        //biome of the stump
        Biome biome = world.getBiomeForCoordsBody(pos);

        //default log
        IBlockState log = Blocks.LOG.getDefaultState();

        //should it generate along the x or z axis?
        Boolean isZ= rand.nextBoolean();

        //should the stump be on the other side?
        Boolean otherSide = rand.nextBoolean();

        //distance of the log from the stump
        int dist = 2+rand.nextInt(2);

        //is this a valid biome?
        if (biome instanceof BiomeForest||biome instanceof BiomeForestMutated)
            if (rand.nextBoolean()||biome== Biomes.BIRCH_FOREST||biome== Biomes.BIRCH_FOREST_HILLS||biome== Biomes.MUTATED_BIRCH_FOREST||biome== Biomes.MUTATED_BIRCH_FOREST_HILLS)
                log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH);
        if (biome instanceof BiomeJungle)
            log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE);
        if (biome instanceof BiomeTaiga)
            log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE);
        if (biome instanceof BiomeTemperateJungle)
            log = logs[rand.nextInt(4)];
        if (biome instanceof BiomePlains || biome instanceof BiomeHills|| biome instanceof BiomeBeach|| biome instanceof BiomeDesert||biome instanceof BiomeSavanna||biome instanceof BiomeSavannaMutated||biome instanceof BiomeMesa||biome instanceof BiomeSaltFlat)
            return;

        //is the position valid?
        BlockPos newPos = pos;
        for (int a=0;a<length+dist;a++){
            if (isZ)
                //shorthand if statement
                newPos = (otherSide) ? pos.subtract(new Vec3i(0,-1,a)) : pos.add(0, 1, a);
            else
                newPos = (otherSide) ? pos.subtract(new Vec3i(a, -1, 0)) : pos.add(a, 1, 0);
            if (!modWorld.isPosEmpty(world, newPos)
                    || world.getBlockState(newPos.down()).getBlock() == Blocks.AIR
                    ||world.getBlockState(newPos.down()).getBlock() == Blocks.WATER)
                return;
        }

        //generate the stump
        world.setBlockState(pos,Blocks.DIRT.getDefaultState());
        world.setBlockState(pos.up(), log);

        //generate vines around the stump
        if (modWorld.isPosEmpty(world, pos.west().up()))
            world.setBlockState(pos.west().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.EAST, true));

        if (modWorld.isPosEmpty(world, pos.east().up()))
            world.setBlockState(pos.east().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.WEST, true));

        if (modWorld.isPosEmpty(world, pos.north().up()))
            world.setBlockState(pos.north().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.SOUTH, true));

        if (modWorld.isPosEmpty(world, pos.south().up()))
            world.setBlockState(pos.south().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.NORTH, true));

        //generate the log
        BlockPos logpos = null;
        for (int a=dist;a<length+dist;a++){
            if (isZ) {
                logpos = (otherSide) ? pos.subtract(new Vec3i(0,-1,a)) : pos.add(0, 1, a);
                world.setBlockState(logpos, log.withProperty(BlockLog.LOG_AXIS, BlockLog.EnumAxis.Z));
            }
            else {
                logpos = (otherSide) ? pos.subtract(new Vec3i(a, -1, 0)) : pos.add(a, 1, 0);
                world.setBlockState(logpos, log.withProperty(BlockLog.LOG_AXIS, BlockLog.EnumAxis.X));
            }
            world.setBlockState(logpos.down(), Blocks.DIRT.getDefaultState());
        }
    }
}
