package net.paradisemod.world.gen.misc;

import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.modWorld;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class Rose implements IWorldGenerator{

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		for (int i=0;i<5;i++) {
			int blockX = (chunkX * 16)+rand.nextInt(16);
			int blockZ = (chunkZ * 16)+rand.nextInt(16);
			int y = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ,Blocks.GRASS);
			BlockPos pos = new BlockPos(blockX, y+1, blockZ);
			if (y<40) return;
			world.setBlockState(pos, Decoration.BlueRose.getDefaultState());
			if (rand.nextBoolean())
				world.setBlockState(pos, Decoration.Rose.getDefaultState());
		}
	}
}