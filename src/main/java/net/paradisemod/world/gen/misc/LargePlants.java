package net.paradisemod.world.gen.misc;

import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.*;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.*;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.world.biome.BiomeRockyDesert;
import net.paradisemod.world.biome.BiomeTemperateJungle;
import net.paradisemod.world.dimension.DimensionRegistry;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class LargePlants implements IWorldGenerator {
	public static final IBlockState AIR = Blocks.AIR.getDefaultState();
	public static final IBlockState DIRT = Blocks.DIRT.getDefaultState();
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		if (world.provider.getDimension()!= DimensionRegistry.DeepUnderground)
			return;
		int[] heights = {127,80,60};
		for (int i=0;i<25;i++) {
			int blockX = (chunkX * 16)+ 3 + rand.nextInt(10);
			int blockZ = (chunkZ * 16)+ 3 + rand.nextInt(10);
			int y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.GRASS);
			BlockPos position = new BlockPos(blockX, y+1, blockZ);
			if (y>31) {
				Biome biome = world.getBiomeForCoordsBody(position);
				// temperate rainforest trees
				if (biome instanceof BiomeTemperateJungle){
					if (rand.nextBoolean())
						genSpruceTree(world,rand,position);
					else
						genClassicTree(world, rand, position,1+rand.nextInt(2),true);
				}

				// oak or birch tree (forest biomes)
				if (biome== Biomes.FOREST||biome== Biomes.FOREST_HILLS||biome== Biomes.MUTATED_FOREST) {
					if (rand.nextInt(10)==0)
						genClassicTree(world, rand, position,1,false);
					else
						genClassicTree(world, rand, position,0,false);
				}
					
				// swamp oak tree
				if (biome instanceof BiomeSwamp)
					genClassicTree(world, rand, position,0,true);
					
				// birch tree
				if(biome == Biomes.BIRCH_FOREST||biome == Biomes.BIRCH_FOREST_HILLS||biome == Biomes.MUTATED_BIRCH_FOREST||biome == Biomes.MUTATED_BIRCH_FOREST_HILLS)
					genClassicTree(world, rand, position,1,false);
					
				// jungle tree
				if(biome instanceof BiomeJungle) {
					genClassicTree(world, rand, position,2,true);
					//generate some bushes
					for (int j=0;j<5;j++) {
						blockX = (chunkX * 16)+ 3 + rand.nextInt(10);
						blockZ = (chunkZ * 16)+ 3 + rand.nextInt(10);
						y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.GRASS);
						position = new BlockPos(blockX, y+1, blockZ);
						if (y>30)
							genJungleBush(world, rand, position);
					}
				}

				// spruce tree
				if(biome instanceof BiomeTaiga)
					genSpruceTree(world, rand, position);
					
				// dark oak tree
				if(biome == Biomes.ROOFED_FOREST || biome == Biomes.MUTATED_ROOFED_FOREST)
					genBigOakTree(world, position,rand);
				
				// acacia tree
				if(biome instanceof BiomeSavanna || biome instanceof BiomeSavannaMutated)
					genAcaciaTree(world, rand, position);

				// cactus
				if(biome instanceof BiomeDesert || biome instanceof BiomeRockyDesert) {
					blockX = (chunkX * 16)+ rand.nextInt(16);
					blockZ = (chunkZ * 16)+ rand.nextInt(16);
					y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, new Block[] {Blocks.SAND, Blocks.SANDSTONE});
					position = new BlockPos(blockX, y, blockZ);
					genCactus(world, position, rand, 0);
				}

				if (biome == Biomes.MESA || biome == Biomes.MESA_CLEAR_ROCK || biome == Biomes.MUTATED_MESA || biome == Biomes.MUTATED_MESA_CLEAR_ROCK || biome == Biomes.MUTATED_MESA_ROCK) {
					blockX = (chunkX * 16)+ rand.nextInt(16);
					blockZ = (chunkZ * 16)+ rand.nextInt(16);
					y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.SAND);
					position = new BlockPos(blockX, y, blockZ);
					genCactus(world, position, rand, 1);
				}
			}
		}
	}

	// cactus
	public static void genCactus(net.minecraft.world.World world, BlockPos pos, Random rand, int sandType) {
		IBlockState[] sand = {
				Blocks.SAND.getDefaultState(),
				Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND)
		};
		int height = rand.nextInt(3);
		for (int y = 0;y <= height; y++)
			if (modWorld.isPosEmpty(world,pos.west().up(y+1))&& modWorld.isPosEmpty(world,pos.east().up(y+1))&& modWorld.isPosEmpty(world,pos.north().up(y+1))&& modWorld.isPosEmpty(world,pos.south().up(y+1))) {
				world.setBlockState(pos, sand[sandType]);
				world.setBlockState(pos.up(y + 1), Blocks.CACTUS.getDefaultState());
			}
			else
				return;
	}

	// cactus
	public static void genCactus(Chunk chunk, BlockPos pos, Random rand, int sandType) {
		IBlockState[] sand = {
				Blocks.SAND.getDefaultState(),
				Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND)
		};
		chunk.setBlockState(pos, sand[sandType]);
		int height = rand.nextInt(3);
		for (int y=0;y<=height;y++)
			if (modWorld.isPosEmpty(chunk,pos.west().up(y+1))&& modWorld.isPosEmpty(chunk,pos.east().up(y+1))&& modWorld.isPosEmpty(chunk,pos.north().up(y+1))&& modWorld.isPosEmpty(chunk,pos.south().up(y+1))) {
				chunk.setBlockState(pos, sand[sandType]);
				chunk.setBlockState(pos.up(y + 1), Blocks.CACTUS.getDefaultState());
			}
			else
				return;
	}
	// acacia trees
	public static void genAcaciaTree(net.minecraft.world.World world, Random rand, BlockPos pos) {
		WorldGenSavannaTree tree = new WorldGenSavannaTree(false);
		tree.generate(world,rand,pos);
	}
	
	// dark oak trees
	public static void genBigOakTree(net.minecraft.world.World world, BlockPos pos, Random rand) {
		WorldGenCanopyTree tree = new WorldGenCanopyTree(false);

		tree.generate(world,rand,pos);
	}

	// jungle bushes
	public static void genJungleBush(net.minecraft.world.World world, Random rand, BlockPos pos) {
		IBlockState log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE);
		IBlockState leaves = Blocks.LEAVES.getDefaultState()
			.withProperty(BlockLeaves.CHECK_DECAY, false)
			.withProperty(BlockLeaves.DECAYABLE, true);

		WorldGenShrub bush = new WorldGenShrub(log,leaves);

		bush.generate(world,rand,pos);
	}

	// oak, birch, and jungle trees
	public static void genClassicTree(net.minecraft.world.World world, Random rand, BlockPos pos, int type, boolean genVines) {
		int height = 5 + rand.nextInt(3);
		if (type == 2)
			height = 6 + rand.nextInt(4);
		IBlockState[] logs = {
				Blocks.LOG.getDefaultState(),
				Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH),
				Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE)
		};
		IBlockState leaves[] = {
				Blocks.LEAVES.getDefaultState().withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true),
				Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.BIRCH).withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true),
				Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.JUNGLE).withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true)
		};

		WorldGenTrees tree = new WorldGenTrees(false,height,logs[type],leaves[type],genVines);

		tree.generate(world, rand,pos);
	}
	
	// spruce trees
	public static void genSpruceTree(net.minecraft.world.World world, Random rand, BlockPos pos) {
		WorldGenTaiga1 spruce = new WorldGenTaiga1();
		spruce.generate(world,rand,pos);
	}
}