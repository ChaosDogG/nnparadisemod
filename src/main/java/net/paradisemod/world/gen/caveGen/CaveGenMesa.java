package net.paradisemod.world.gen.caveGen;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSand;
import net.minecraft.block.BlockStainedHardenedClay;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeMesa;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.modWorld;
import net.paradisemod.world.gen.misc.LargePlants;

import java.util.Random;

public class CaveGenMesa implements IWorldGenerator {
	protected static final IBlockState TERRACOTTA = Blocks.HARDENED_CLAY.getDefaultState();
	protected static final IBlockState ORANGE_TERRACOTTA = Blocks.STAINED_HARDENED_CLAY.getDefaultState()
			.withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.ORANGE);
	protected static final IBlockState WHITE_TERRACOTTA = Blocks.STAINED_HARDENED_CLAY.getDefaultState()
			.withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.WHITE);
	protected static final IBlockState BROWN_TERRACOTTA = Blocks.STAINED_HARDENED_CLAY.getDefaultState()
			.withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.BROWN);
	protected static final IBlockState YELLOW_TERRACOTTA = Blocks.STAINED_HARDENED_CLAY.getDefaultState()
			.withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.YELLOW);
	protected static final IBlockState RED_TERRACOTTA = Blocks.STAINED_HARDENED_CLAY.getDefaultState()
			.withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.RED);
	public static int height;

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator,
			IChunkProvider chunkProvider) {
		Chunk chunk = world.getChunkFromChunkCoords(chunkX, chunkZ);

		// don't generate if the config says not to
		// generate cave features
		if (!ModConfig.worldgen.caves.betterCaves)
			return;
		if (!ModConfig.worldgen.caves.types.Mesa)
			return;

		// the height is 256 if the features are being generated in a cave dimension,
		// else, it's 55
		height = 255;
		if (world.provider.getDimension() == 0) {
			height = 55;
		}

		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 0; y < height; y++) {
					BlockPos pos = new BlockPos(x,y,z);

					// biome of current block
					Biome blockBiome = chunk.getBiome(pos, world.getBiomeProvider());

					// the block to be replaced
					Block blockToReplace = chunk.getBlockState(pos).getBlock();

					// the block above it
					Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

					if (blockBiome instanceof BiomeMesa) {
						// replace exposed stone
						if ((blockToReplace == Blocks.STONE || blockToReplace == Blocks.COBBLESTONE)
								&& (chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

								|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

								|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA)) {
							// terracotta
							switch (y) {
								case 74:
								case 45:
									chunk.setBlockState(pos, ORANGE_TERRACOTTA);
									break;

								case 79:
								case 50:
									chunk.setBlockState(pos, WHITE_TERRACOTTA);
									break;

								case 83:
								case 55:
									chunk.setBlockState(pos, BROWN_TERRACOTTA);
									break;

								case 85:
								case 64:
									chunk.setBlockState(pos, YELLOW_TERRACOTTA);
									break;

								case 40:
								case 56:
									chunk.setBlockState(pos, RED_TERRACOTTA);
									break;

								default:
									chunk.setBlockState(pos, TERRACOTTA);
							}
						}

						// this needs to be reset
						blockToReplace = chunk.getBlockState(x, y, z).getBlock();

						// shrubs and cactus
						if (blockToReplace == Blocks.STAINED_HARDENED_CLAY || blockToReplace == Blocks.HARDENED_CLAY || blockToReplace == Blocks.RED_SANDSTONE || blockToReplace == Blocks.SAND || blockToReplace == Blocks.STONE && blockAbove==Blocks.AIR){
							if (rand.nextInt(10) == 0) {
								if (rand.nextBoolean()) {
									if (modWorld.isPosEmpty(chunk,pos.up())) {
										if (blockToReplace !=  Blocks.STAINED_HARDENED_CLAY && blockToReplace != Blocks.HARDENED_CLAY && blockToReplace != Blocks.SAND) chunk.setBlockState(pos, Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND));
										chunk.setBlockState(pos.up(), Blocks.DEADBUSH.getDefaultState());
									}
								}
								else {
									LargePlants.genCactus(chunk, pos, rand, 1);
								}
							}
						}
					}
				}
			}
		}
	}
}