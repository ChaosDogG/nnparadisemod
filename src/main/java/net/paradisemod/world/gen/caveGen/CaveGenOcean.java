package net.paradisemod.world.gen.caveGen;

import net.paradisemod.base.ModConfig;
import net.paradisemod.misc.Misc;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeOcean;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

// water filled caves in oceans in the overworld
public class CaveGenOcean implements IWorldGenerator{
	protected static final IBlockState WATER = Blocks.WATER.getDefaultState();
	public static int height;
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator,IChunkProvider chunkProvider) {
		// don't generate if the config says not to
		// generate cave features
		if (ModConfig.worldgen.caves.betterCaves == false)
			return;
		if (ModConfig.worldgen.caves.types.Ocean== false)
			return;
		Chunk chunk=world.getChunkFromChunkCoords(chunkX, chunkZ);


		// the height is 256 if the features are being generated in a cave dimension,
		// else, it's 61
		height=255;
		int dim = world.provider.getDimension();
		if (dim == 0)
			height = 61;

		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 0; y < height; y++) {
					BlockPos pos = new BlockPos(x,y,z);

					// biome of current block
					Biome blockBiome = chunk.getBiome(pos, world.getBiomeProvider());

					// the block to be replaced
					Block blockToReplace = chunk.getBlockState(pos).getBlock();

					// the block below it
					Block blockBelow = chunk.getBlockState(pos.down()).getBlock();

					if (blockToReplace==Blocks.AIR&& (blockBiome instanceof BiomeOcean)&& dim == 0) {
						chunk.setBlockState(pos, WATER);
						if (blockBelow == Blocks.LAVA) {
							chunk.setBlockState(pos.down(), Blocks.OBSIDIAN.getDefaultState());
							if (rand.nextBoolean())
								chunk.setBlockState(pos.down(), Blocks.MAGMA.getDefaultState());
						}
					}

					if ((blockBiome instanceof BiomeOcean)&&(blockToReplace == Blocks.STONE||blockToReplace == Blocks.PRISMARINE) && 
							(chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
									|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
									|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
									|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
									||chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
									|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

									|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
									|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
									|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
									|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
									|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
									|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

									|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
									|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
									|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
									|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
									|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
									|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA)) {
						if (rand.nextInt(5)==0)
							chunk.setBlockState(pos, Blocks.PRISMARINE.getDefaultState());
						if (rand.nextInt(ModConfig.worldgen.caves.CrystalChance)==0)
							chunk.setBlockState(pos, Misc.prismarineCrystalBlock.getDefaultState());
					}
				}
			}
		}	
	}
}