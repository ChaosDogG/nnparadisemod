package net.paradisemod.world.gen.caveGen;

import net.paradisemod.base.ModConfig;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.biome.BiomeGlacier;
import net.paradisemod.world.biome.BiomeRegistry;
import net.paradisemod.world.modWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeSnow;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class CaveGenIcy implements IWorldGenerator{
	protected static final IBlockState PACKED_ICE = Blocks.PACKED_ICE.getDefaultState();
	public static int height;

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator,IChunkProvider chunkProvider) {
		Chunk chunk=world.getChunkFromChunkCoords(chunkX, chunkZ);

		// don't generate if the config says not to
		// generate cave features
		if (!ModConfig.worldgen.caves.betterCaves)
			return;
		if (!ModConfig.worldgen.caves.types.Icy)
			return;

		// the height is 256 if the features are being generated in a cave dimension,
		// else, it's 61
		height=255;
		if (world.provider.getDimension() == 0)
			height = 61;

		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 0; y < height; y++) {
					BlockPos pos = new BlockPos(x,y,z);

					// biome of current block
					Biome blockBiome = chunk.getBiome(pos, world.getBiomeProvider());

					// the block to be replaced
					Block blockToReplace = chunk.getBlockState(pos).getBlock();

					// the block above it
					Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

					// the block below it
					Block blockBelow = chunk.getBlockState(pos.down()).getBlock();

					if (
							(
									blockToReplace == Blocks.STONE
									|| blockToReplace == Blocks.DIRT
									|| blockToReplace == PACKED_ICE
							)
							&&(
									blockBiome instanceof BiomeSnow
									|| blockBiome == Biomes.FROZEN_RIVER
									|| blockBiome == Biomes.COLD_TAIGA
									|| blockBiome == Biomes.COLD_TAIGA_HILLS
									|| blockBiome == Biomes.COLD_BEACH
									|| blockBiome instanceof BiomeGlacier
									|| blockBiome == BiomeRegistry.snowyRockyDesert
									|| blockBiome == BiomeRegistry.snowyRockyDesertHills
							)
					) {
						
						// replace exposed stone
						if (chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
								||chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

								|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

								|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
								|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA) {

							//icicles
							if (blockAbove==Blocks.AIR&&rand.nextInt(10)==0)
								chunk.setBlockState(pos.up(), modWorld.icicle.getDefaultState());
							if (blockBelow==Blocks.AIR&&rand.nextInt(10)==0&&y>0)
								chunk.setBlockState(pos.down(), modWorld.icicle.getDefaultState().withProperty(BlockDirectional.FACING, EnumFacing.DOWN));

							// ice cave generation
							switch (rand.nextInt(5)) {
								//packed ice
								case 1:
									chunk.setBlockState(pos, PACKED_ICE);
									break;
								//snow
								case 4:
									if(blockAbove==Blocks.AIR)
										chunk.setBlockState(pos.up(),Blocks.SNOW_LAYER.getDefaultState());
									break;
							}
						}
						
						//icicles
						if (blockToReplace == Misc.blueIce&&blockBiome instanceof BiomeGlacier) {
							if (blockAbove==Blocks.AIR&&rand.nextInt(10)==0)
								chunk.setBlockState(pos.up(), modWorld.icicle.getDefaultState());
							if (blockBelow==Blocks.AIR&&rand.nextInt(10)==0&&y>0)
								chunk.setBlockState(pos.down(), modWorld.icicle.getDefaultState().withProperty(BlockDirectional.FACING, EnumFacing.DOWN));
						}
					}
				}
			}
		}
	}
}