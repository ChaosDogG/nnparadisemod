package net.paradisemod.world.biome;

import net.paradisemod.world.modWorld;
import net.paradisemod.world.gen.misc.CustomLakes;
import net.paradisemod.world.gen.misc.LargePlants;
import net.minecraft.block.BlockStone;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.block.Block;

import java.util.Random;

public class BiomeVolcanic extends Biome {
	public static final IBlockState[] topBlocks = {
			Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT, BlockStone.EnumType.ANDESITE),
			Blocks.MAGMA.getDefaultState(),
			Blocks.OBSIDIAN.getDefaultState()
	};
	public static final Block[] ground = {Blocks.STONE,Blocks.DIRT,Blocks.MAGMA,Blocks.OBSIDIAN};
	public BiomeVolcanic(BiomeProperties properties) {
		super(properties);
		decorator.treesPerChunk = 0;
		decorator.grassPerChunk = 0;
		decorator.flowersPerChunk = 0;
		decorator.cactiPerChunk = 0;
		decorator.clayPerChunk = 1;
		decorator.deadBushPerChunk = 5;
		decorator.mushroomsPerChunk = 0;
		topBlock = Blocks.STONE.getDefaultState();
		fillerBlock = Blocks.STONE.getDefaultState();

		//don't spawn animals here
		spawnableCreatureList.clear();
	}

	public void decorate(net.minecraft.world.World world, Random rand, BlockPos pos)
	{
		super.decorate(world, rand, pos);
		int blockX = pos.getX()+rand.nextInt(8);
		int blockZ = pos.getZ()+rand.nextInt(8);
		int y = 0;
		BlockPos pos2 = new BlockPos(blockX, y, blockZ);

		//magma, obsidian and andesite generation
		for (int i=0;i<50;i++) {
			blockX = pos.getX()+rand.nextInt(8);
			blockZ = pos.getZ()+rand.nextInt(8);
			modWorld.generateOre(topBlocks[rand.nextInt(3)],world, rand, blockX,blockZ,5,150,20,16,Blocks.STONE);
		}

		//lava lakes
		blockX = pos.getX()+rand.nextInt(8);
		blockZ = pos.getZ()+rand.nextInt(8);
		y = modWorld.getGroundFromAbove(world, 2, 255, blockX, blockZ, ground);
		if (y>29)
			CustomLakes.genlake(world, rand, blockX, y, blockZ, Blocks.LAVA);

		//cactus generation
		for (int i=0;i<3;i++) {
			blockX = pos.getX()+rand.nextInt(8);
			blockZ = pos.getZ()+rand.nextInt(8);
			y = modWorld.getGroundFromAbove(world, 2, 255, blockX, blockZ, Blocks.DIRT);
			pos2 = new BlockPos(blockX, y, blockZ);
			if (y>0)
				LargePlants.genCactus(world, pos2, rand, 0);
		}
	}
}