package net.paradisemod.world.biome;

import net.paradisemod.world.modWorld;
import net.paradisemod.world.gen.misc.LargePlants;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.monster.EntityHusk;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenTaiga1;
import net.minecraft.world.gen.feature.WorldGenTrees;

import java.util.Random;

public class BiomeRockyDesert  extends Biome{
	public static final WorldGenAbstractTree oak = new WorldGenTrees(false,3, Blocks.LOG.getDefaultState(), Blocks.LEAVES.getDefaultState()
			.withProperty(BlockLeaves.CHECK_DECAY, false)
			.withProperty(BlockLeaves.DECAYABLE, false),false);
	public static final WorldGenAbstractTree spruce = new WorldGenTaiga1();
	public static final IBlockState[] topBlocks = {
			Blocks.DIRT.getDefaultState().withProperty(BlockDirt.VARIANT, BlockDirt.DirtType.COARSE_DIRT),
			Blocks.GRAVEL.getDefaultState()
	};

	public BiomeRockyDesert(BiomeProperties properties) {
		super(properties);
		decorator.treesPerChunk = 1;
		decorator.grassPerChunk = 3;
		decorator.flowersPerChunk = 0;
		decorator.cactiPerChunk = 0;
		decorator.clayPerChunk = 1;
		decorator.deadBushPerChunk = 10;
		decorator.mushroomsPerChunk = 0;
		topBlock = Blocks.DIRT.getDefaultState();
		fillerBlock = Blocks.DIRT.getDefaultState();
		spawnableMonsterList.add(new Biome.SpawnListEntry(EntityHusk.class, 95, 4, 4));
	}
	public int getModdedBiomeGrassColor(int original) {
		if (getDefaultTemperature() <= 1)
			return 0x97ff4d;
		else
			return 0xffcc4d;
	}

	public void decorate(net.minecraft.world.World world, Random rand, BlockPos pos){
		super.decorate(world, rand, pos);
		int blockX;
		int blockZ;
		int y = 0;

		// patches of gravel and coarse dirt
		for (int i = 0; i < 3; i++) {
			blockX = pos.getX()+rand.nextInt(8);
			blockZ = pos.getZ()+rand.nextInt(8);
			modWorld.generateOre(topBlocks[rand.nextInt(2)],world, rand, blockX,blockZ,5,150,20,16,Blocks.DIRT);
		}

		// foliage generation
		for (int i = 0; i < 64; i++) {
			blockX = pos.getX()+rand.nextInt(16);
			blockZ = pos.getZ()+rand.nextInt(16);
			y = modWorld.getGroundFromAbove(world, 2, 255, blockX, blockZ, Blocks.DIRT);
			BlockPos pos2 = new BlockPos(blockX, y, blockZ);
			if (y >= 31 && !(world.getBlockState(pos2).getBlock() instanceof BlockLiquid)) {
				switch (rand.nextInt(3)) {
					case 0:
						world.setBlockState(pos2.up(), Blocks.DEADBUSH.getDefaultState());
						break;

					case 1:
						LargePlants.genCactus(world, pos2, rand, 0);
						break;

					case 2:
						world.setBlockState(pos2.up(), Blocks.TALLGRASS.getDefaultState().withProperty(BlockTallGrass.TYPE, BlockTallGrass.EnumType.GRASS));
				}
			}
		}
	}

	public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
		return (getDefaultTemperature() < 1 && rand.nextBoolean()) ? spruce : oak;
	}
}