package net.paradisemod.world.biome;

import net.paradisemod.world.blocks.fluids.MoltenSalt;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.Ores;
import net.paradisemod.world.gen.misc.CustomLakes;
import net.minecraft.entity.monster.EntityHusk;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.util.Random;

public class BiomeSaltFlat  extends Biome {

	public BiomeSaltFlat(BiomeProperties properties) {
		super(properties);
		properties.setWaterColor(0xc29191);
		decorator.treesPerChunk = 0;
		decorator.grassPerChunk = 0;
		decorator.flowersPerChunk = 0;
		decorator.cactiPerChunk = 0;
		decorator.clayPerChunk = 0;
		decorator.deadBushPerChunk = 0;
		decorator.mushroomsPerChunk = 0;
		topBlock = Ores.saltBlock2.getDefaultState();
		fillerBlock = Ores.saltBlock2.getDefaultState();
		spawnableMonsterList.add(new Biome.SpawnListEntry(EntityHusk.class, 95, 4, 4));
	}
	public int getWaterColorMultiplier()
	{
		return 0xffbdbd;
	}

	// this is the world generation for the biome
	public void decorate(World world, Random rand, BlockPos pos)
	{
		int dim = world.provider.getDimension();
		int blockX = pos.getX()+rand.nextInt(8);
		int blockZ = pos.getZ()+rand.nextInt(8);
		int y = 0;

		// molten salt lakes
		if (rand.nextInt(3)==0) {
			blockX = pos.getX() + rand.nextInt(8);
			blockZ = pos.getZ() + rand.nextInt(8);
			y = 30 + rand.nextInt(40);
			if (dim== ModConfig.dimensions.DeepUndergroundDim)
				y = 30 + rand.nextInt(98);
			if (y > 29)
				CustomLakes.genlake(world, rand, blockX, y, blockZ, MoltenSalt.BlockMoltenSalt.instance);
		}
	}
}