package net.paradisemod.world;

import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.*;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.biome.BiomeRegistry;
import net.paradisemod.world.blocks.*;
import net.paradisemod.world.blocks.fluids.*;
import net.paradisemod.world.dimension.DimensionRegistry;
import org.apache.logging.log4j.Level;

import java.util.Arrays;
import java.util.Random;

public class modWorld {
    // cave formations
    public static CaveFormation icicle = new CaveFormation(SoundType.GLASS);
    public static CaveFormation mossyStoneFormation = new CaveFormation(SoundType.STONE);
    public static CaveFormation netherrackFormation = new CaveFormation(SoundType.STONE);
    public static CaveFormation sandstoneFormation = new CaveFormation(SoundType.STONE);
    public static CaveFormation stoneFormation = new CaveFormation(SoundType.STONE);
    public static CaveFormation voidFormation = new CaveFormation(SoundType.STONE);

    // new end blocks
    public static Block OvergrownEndStone = new OvergrownEndStone();
    public static CustomEndPlant EndGrass = new CustomEndPlant(true);
    public static CustomEndPlant TallEndGrass = new CustomEndPlant(true);

    // portal blocks
    public static net.paradisemod.world.blocks.DUPortal DUPortal = new DUPortal();
    public static net.paradisemod.world.blocks.DVPortal DVPortal = new DVPortal();

    // blocks that fallen trees can replace
    public static Block[] replaceable = {
            Blocks.AIR,
            Blocks.TALLGRASS,
            Blocks.DOUBLE_PLANT,
            Decoration.BlueRose,
            Decoration.Rose,
            Blocks.VINE,
            Blocks.RED_FLOWER,
            Blocks.YELLOW_FLOWER,
            Blocks.LEAVES,
            Blocks.LEAVES2,
            sandstoneFormation,
            mossyStoneFormation,
            stoneFormation,
            netherrackFormation,
            voidFormation,
            Blocks.WATER,
            Blocks.FLOWING_WATER,
            Blocks.SNOW_LAYER
    };

    // blocks that normally make up the ground in Minecraft
    public static final Block[] ground = {Blocks.GRASS, Blocks.DIRT, Blocks.MYCELIUM, Blocks.SAND, Blocks.GRAVEL};
    public static final Block[] endGround = {Blocks.END_STONE, OvergrownEndStone};
    public static final Block[] mesaGround = {Blocks.STAINED_HARDENED_CLAY, Blocks.RED_SANDSTONE};

    // the cardinal directions
    public static final String[] dirStrings= {"east","west","north","south"};

    // common plants
    public static final IBlockState[] plants = {Blocks.TALLGRASS.getDefaultState().withProperty(BlockTallGrass.TYPE, BlockTallGrass.EnumType.GRASS), Blocks.YELLOW_FLOWER.getDefaultState(), Blocks.RED_FLOWER.getDefaultState(), Decoration.Rose.getDefaultState(), Decoration.BlueRose.getDefaultState()};

    public static void init(){
        // event handlers
        MinecraftForge.EVENT_BUS.register(Events.class);

        // fluids
        LiquidRedstone.register();
        EnderAcid.register();
        GlowingWater.register();
        MoltenSalt.register();

        // register the custom renderer for the fluids
        if (ParadiseMod.proxy.isClient()) {
            fluidRenders.register();
        }

        // world generators
        worldGen.init();

        // biomes
        BiomeRegistry.regBiomes();

        // dimensions
        DimensionRegistry.registerDimensions();

        // spawn mobs in the deep void dimension
        EntityRegistry.addSpawn(EntityZombie.class, 10, 0, 10, EnumCreatureType.MONSTER, Biomes.VOID);
        EntityRegistry.addSpawn(EntityCreeper.class, 10, 0, 10, EnumCreatureType.MONSTER, Biomes.VOID);
        EntityRegistry.addSpawn(EntitySpider.class, 10, 0, 10, EnumCreatureType.MONSTER, Biomes.VOID);
        EntityRegistry.addSpawn(EntitySkeleton.class, 10, 0, 10, EnumCreatureType.MONSTER, Biomes.VOID);
        EntityRegistry.addSpawn(EntityWitherSkeleton.class, 1, 0, 10, EnumCreatureType.MONSTER, Biomes.VOID);
        EntityRegistry.addSpawn(EntityEnderman.class, 1, 0, 1, EnumCreatureType.MONSTER, Biomes.VOID);

        // fluid buckets
        FluidRegistry.addBucketForFluid(LiquidRedstone.FluidLiquidRedstone.instance);
        FluidRegistry.addBucketForFluid(EnderAcid.FluidEnderAcid.instance);
        FluidRegistry.addBucketForFluid(GlowingWater.FluidGlowingWater.instance);
        FluidRegistry.addBucketForFluid(MoltenSalt.FluidMoltenSalt.instance);

        // crystals
        Crystals.init();

        // ores and resource blocks
        Ores.init();

        // cave formations
        Utils.regBlock(icicle.setUnlocalizedName("Icicle").setRegistryName("icicle"));
        Utils.regBlock(mossyStoneFormation.setUnlocalizedName("mossyStoneFormation").setRegistryName("mossy_stone_formation"));
        Utils.regBlock(netherrackFormation.setUnlocalizedName("netherrackFormation").setRegistryName("netherrack_formation"));
        Utils.regBlock(sandstoneFormation.setUnlocalizedName("sandstoneFormation").setRegistryName("sandstone_formation"));
        Utils.regBlock(voidFormation.setUnlocalizedName("voidStoneFormation").setRegistryName("void_stone_formation"));
        Utils.regBlock(stoneFormation.setUnlocalizedName("stoneFormation").setRegistryName("stone_formation"));

        // new end blocks
        Utils.regBlock(OvergrownEndStone);
        Utils.regBlock(EndGrass.setUnlocalizedName("EndGrass").setRegistryName("end_grass"));
        Utils.regBlock(TallEndGrass.setUnlocalizedName("TallEndGrass").setRegistryName("tall_end_grass"));

        // portal blocks
        Utils.regBlock(DUPortal);
        Utils.regBlock(DVPortal);

        ParadiseMod.LOG.log(Level.INFO,"Loaded world module");
    }

    public static void regRenders() {
        Utils.regRender(sandstoneFormation);
        Utils.regRender(stoneFormation);
        Utils.regRender(voidFormation);
        Utils.regRender(icicle);
        Utils.regRender(mossyStoneFormation);
        Utils.regRender(netherrackFormation);
        Utils.regRender(OvergrownEndStone);
        Utils.regRender(EndGrass);
        Utils.regRender(TallEndGrass);
        Utils.regRender(DUPortal);
        Utils.regRender(DVPortal);

        Crystals.regRenders();
        Ores.regRenders();
    }
    //generate an ore cluster in a specific block layer(like stone)
    public static void generateOre(IBlockState ore, net.minecraft.world.World world, Random random, int x, int z, int minY, int maxY, int size, int chances, Block layer) {
        int deltaY = maxY - minY;
        int rarity = 16;
        if (ore== Ores.RubyOre.getDefaultState())
            rarity=8;
        for (int i = 0; i < chances; i++) {
            BlockPos pos = new BlockPos(x + random.nextInt(rarity), minY + random.nextInt(deltaY), z + random.nextInt(8));
            WorldGenMinable generator = new WorldGenMinable(ore, size, BlockMatcher.forBlock(layer));
            generator.generate(world, random, pos);
        }
    }

    //Returns a random Stone brick block state object
    public static IBlockState getRandomBrick(Random rand) {
        return getRandomBrick(rand, false);
    }
    public static IBlockState getRandomBrick(Random rand, Boolean isVoid) {
        if (isVoid)
            return Building.VoidBricks.getDefaultState();
        IBlockState[] bricks = {
                Blocks.STONEBRICK.getDefaultState().withProperty(BlockStoneBrick.VARIANT, BlockStoneBrick.EnumType.DEFAULT),
                Blocks.STONEBRICK.getDefaultState().withProperty(BlockStoneBrick.VARIANT, BlockStoneBrick.EnumType.CRACKED),
                Blocks.STONEBRICK.getDefaultState().withProperty(BlockStoneBrick.VARIANT, BlockStoneBrick.EnumType.MOSSY)
        };
        return bricks[rand.nextInt(3)];
    }

    // find ground to generate a structure or place a block
    public static int getGroundFromAbove(net.minecraft.world.World world, int minY, int maxY, int x, int z, Block[] groundBlocks, boolean isUnderwater)
    {
        int y = maxY;
        boolean foundGround = false;
        while(!foundGround && y-->= minY)
        {
            Block blockAt = world.getBlockState(new BlockPos(x,y,z)).getBlock();
            Block blockAbove = world.getBlockState(new BlockPos(x,y+1,z)).getBlock();
            foundGround=(Arrays.asList(groundBlocks).contains(blockAt)  && blockAbove== Blocks.AIR) || (Arrays.asList(groundBlocks).contains(blockAt)  && blockAbove==Blocks.SNOW_LAYER);
            if (isUnderwater)
                foundGround=(Arrays.asList(groundBlocks).contains(blockAt)  && blockAbove==Blocks.WATER);
        }
        return y;
    }

    public static int getGroundFromAbove(net.minecraft.world.World world, int minY, int maxY, int x, int z, Block groundBlock) {
        return getGroundFromAbove(world, minY, maxY, x, z, new Block [] {groundBlock},false);
    }

    public static int getGroundFromAbove(net.minecraft.world.World world, int minY, int maxY, int x, int z, Block[] groundBlocks) {
        return getGroundFromAbove(world, minY, maxY, x, z, groundBlocks, false);
    }

    public static int getGroundFromAbove(net.minecraft.world.World world, int minY, int maxY, int x, int z) {
        return getGroundFromAbove(world, minY, maxY, x, z, ground, false);
    }

    // check is block position is occupied by an opaque block
    public static Boolean isPosEmpty(net.minecraft.world.World world, BlockPos pos){
        return Arrays.asList(replaceable).contains(world.getBlockState(pos).getBlock()) && !(world.getBlockState(pos).getBlock() instanceof BlockLiquid);
    }

    // same thing as above, but with the Chunk object instead of the World object
    public static Boolean isPosEmpty(Chunk chunk, BlockPos pos){
        return Arrays.asList(replaceable).contains(chunk.getBlockState(pos).getBlock()) && !(chunk.getBlockState(pos).getBlock() instanceof BlockLiquid);
    }
}
