package net.paradisemod.misc.items.tools.obsidian;

import net.paradisemod.base.Reference;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.util.ResourceLocation;

public class obsidianPickaxe extends ItemPickaxe {
	public obsidianPickaxe(ToolMaterial material, String unlocalizedName) {
		super(material);
		setUnlocalizedName(unlocalizedName);
		setRegistryName(new ResourceLocation(Reference.MOD_ID, unlocalizedName));
	}
}