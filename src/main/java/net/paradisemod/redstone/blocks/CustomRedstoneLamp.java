package net.paradisemod.redstone.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRedstoneLight;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class CustomRedstoneLamp extends BlockRedstoneLight {
    public final Boolean isLitLamp;
    private Block unlitLamp = Blocks.AIR;
    private Block litLamp = Blocks.AIR;
    private final String lamp_color;

    public CustomRedstoneLamp(String color, String colorName, Boolean isLit) {
        super(isLit);
        isLitLamp = isLit;
        lamp_color = color;
        setUnlocalizedName(colorName + "RSLamp");
        if (isLit) {
            setLightLevel(1F);
            setRegistryName("lit_" + color + "_redstone_lamp");
        }
        else setRegistryName(color + "_redstone_lamp");
        setSoundType(SoundType.GLASS);
        setCreativeTab(CreativeTabs.REDSTONE);
        if (isLit) setLightLevel(1F);
    }
    
    public void setUnlitLamp(Block lamp) {
        unlitLamp = lamp;
    }
    
    public void setLitLamp(Block lamp) {
        litLamp = lamp;
    }

    @Override
    public void onBlockAdded(World world, BlockPos pos, IBlockState state)
    {
        if (!world.isRemote) toggle(world, pos);
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        if (!world.isRemote) toggle(world, pos);
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        if (!world.isRemote) toggle(world, pos);
    }

    private void toggle(World world, BlockPos pos) {
        if (isLitLamp && !world.isBlockPowered(pos)) {
            world.setBlockState(pos, unlitLamp.getDefaultState(), 2);
        }
        else if (!isLitLamp && world.isBlockPowered(pos)){
            world.setBlockState(pos, litLamp.getDefaultState(), 2);
        }
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(unlitLamp);
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
    {
        return new ItemStack(unlitLamp);
    }

    // bonus features for the golden redstone lamp
    @Override
    public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn) {
        if (isLitLamp && lamp_color.equals("gold")) {
            EntityLivingBase entityLiving = (EntityLivingBase) entityIn;
            entityLiving.addPotionEffect(new PotionEffect(Potion.getPotionById(10), 100));
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (isLitLamp && lamp_color.equals("gold")) {
            for (int i = 0; i < 4; ++i) {
                double d0 = pos.getX() + rand.nextFloat();
                double d1 = pos.getY() + rand.nextFloat();
                double d2 = pos.getZ() + rand.nextFloat();
                double d3 = (rand.nextFloat() - 0.5d) * 0.5d;
                double d4 = (rand.nextFloat() - 0.5d) * 0.5d;
                double d5 = (rand.nextFloat() - 0.5d) * 0.5d;
                int j = rand.nextInt(2) * 2 - 1;
                if (worldIn.getBlockState(pos.west()).getBlock() != this && worldIn.getBlockState(pos.east()).getBlock() != this) {
                    d0 = pos.getX() + 0.5d + 0.25d * j;
                    d3 = rand.nextFloat() * 2.0f * j;
                } else {
                    d2 = pos.getZ() + 0.5d + 0.25d * j;
                    d5 = rand.nextFloat() * 2.0f * j;
                }
                worldIn.spawnParticle(EnumParticleTypes.SPELL_MOB_AMBIENT, d0, d1, d2, d3, d4, d5, new int[0]);
            }
        }
    }
}