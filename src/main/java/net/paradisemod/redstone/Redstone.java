package net.paradisemod.redstone;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLever;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.redstone.blocks.mossyLever;
import net.paradisemod.redstone.blocks.spike;
import org.apache.logging.log4j.Level;

public class Redstone {
    public static BlockLever mossyLever = new mossyLever();
    public static Block spike = new spike();
    public static void init() {
        Utils.regBlock(mossyLever);
        Utils.regBlock(spike);

        Buttons.init();
        Lamps.init();
        Plates.init();

        ParadiseMod.LOG.log(Level.INFO,"Loaded redstone module");
    }

    public static void regRenders() {
        Utils.regRender(mossyLever);
        Utils.regRender(spike);

        Buttons.regRenders();
        Lamps.regRenders();
        Plates.regRenders();
    }
}
