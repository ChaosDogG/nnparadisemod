package net.paradisemod.base.proxy;

import net.paradisemod.automation.Automation;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Misc;
import net.paradisemod.misc.tileentity.TileEntityCactusChest;
import net.paradisemod.misc.tileentity.TileEntityCactusChestRender;
import net.paradisemod.misc.tileentity.TileEntityCompressedCactusChest;
import net.paradisemod.misc.tileentity.TileEntityCompressedCactusChestRender;
import net.paradisemod.redstone.Buttons;
import net.paradisemod.redstone.Plates;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.world.modWorld;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ColorizerGrass;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.biome.BiomeColorHelper;
import net.minecraftforge.fml.client.registry.ClientRegistry;

import javax.annotation.Nullable;

public class ClientProxy implements CommonProxy{
	@Override
	public void init() {
		//modules
		Automation.regRenders();
		Bonus.regRenders();
		Building.regRenders();
		Decoration.regRenders();
		Misc.regRenders();
		Redstone.regRenders();
		modWorld.regRenders();

		// renderer for the chests
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCactusChest.class, new TileEntityCactusChestRender());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCompressedCactusChest.class, new TileEntityCompressedCactusChestRender());

		// renderer for the grass button and pressure plate
		Minecraft.getMinecraft().getBlockColors().registerBlockColorHandler(new IBlockColor()
        {
            @Override
			public int colorMultiplier(IBlockState state, @Nullable IBlockAccess worldIn, @Nullable BlockPos pos, int tintIndex)
            {
                return worldIn != null && pos != null ? BiomeColorHelper.getGrassColorAtPos(worldIn, pos) : ColorizerGrass.getGrassColor(0.5D, 1.0D);
            }
        }, Plates.GrassPlate);		
		Minecraft.getMinecraft().getBlockColors().registerBlockColorHandler(new IBlockColor()
        {
            @Override
			public int colorMultiplier(IBlockState state, @Nullable IBlockAccess worldIn, @Nullable BlockPos pos, int tintIndex)
            {
                return worldIn != null && pos != null ? BiomeColorHelper.getGrassColorAtPos(worldIn, pos) : ColorizerGrass.getGrassColor(0.5D, 1.0D);
            }
        }, Buttons.GrassButton);
	}

	@Override
	public boolean isClient() { return true; }
}