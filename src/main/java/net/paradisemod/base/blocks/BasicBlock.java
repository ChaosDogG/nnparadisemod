package net.paradisemod.base.blocks;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
//class for a basic Minecraft block
public class BasicBlock extends Block{

	//should this block be able to be used as a beacon base?
	public final boolean isBeacon;

	public BasicBlock(Material material, SoundType sound, boolean beaconBase) {
		super(material);
		setSoundType(sound);
		isBeacon=beaconBase;
	}

	@Override
    public boolean isBeaconBase(IBlockAccess worldObj, BlockPos pos, BlockPos beacon)
    {
        return isBeacon;
    }
}