package net.paradisemod.base;
public class Reference {
	// Basic Mod Info
	public static final String MOD_ID = "nnparadisemod";
	public static final String NAME = "Nether Noah's Paradise Mod";
	public static final String VERSION = "1.8 (The Modularization)";
	public static final String ACCEPTED_VERSIONS = "1.12.2";

	// Proxy Classes
	public static final String CLIENT_PROXY_CLASS = "net.paradisemod.base.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "net.paradisemod.base.proxy.ServerProxy";
	
	// GUI ID for the custom workbench
	public static int GUI_CUSTOM_WORKBENCH = 0;
}
