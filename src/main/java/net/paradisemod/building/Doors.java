package net.paradisemod.building;

import net.paradisemod.building.blocks.CustomDoor;
import net.paradisemod.building.blocks.RedstoneDoor;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Doors {
	// doors
	public static CustomDoor CactusDoor;
	public static CustomDoor EndDoor;
	public static CustomDoor SilverDoor;
	public static CustomDoor GlassDoor;
	public static CustomDoor GoldDoor;
	public static CustomDoor DiamondDoor;
	public static CustomDoor EmeraldDoor;
	public static CustomDoor AndesiteDoor;
	public static CustomDoor DioriteDoor;
	public static CustomDoor GraniteDoor;
	public static CustomDoor StoneDoor;
	public static CustomDoor CobblestoneDoor;
	public static CustomDoor MossStoneDoor;
	public static CustomDoor GlowingObsidianDoor;
	public static CustomDoor ObsidianDoor;
	public static CustomDoor BedrockDoor;
	public static CustomDoor RubyDoor;
	public static Block RedstoneDoor = new RedstoneDoor();
	public static CustomDoor RustyDoor;

	public static void init() {
		// door blocks
		// they don't need their own items
		ForgeRegistries.BLOCKS.register(AndesiteDoor = new CustomDoor(Material.IRON,DoorItems.AndesiteDoor,"andesite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(BedrockDoor = new CustomDoor(Material.IRON,DoorItems.BedrockDoor,"bedrock_door_block",-1f,6000000F,"pickaxe",6, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(CactusDoor = new CustomDoor(Material.WOOD,DoorItems.CactusDoor,"cactus_door_block",5f,10F,"axe",0,SoundType.WOOD));
		ForgeRegistries.BLOCKS.register(CobblestoneDoor = new CustomDoor(Material.IRON,DoorItems.CobblestoneDoor,"cobblestone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(DiamondDoor = new CustomDoor(Material.IRON,DoorItems.DiamondDoor,"diamond_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(DioriteDoor = new CustomDoor(Material.IRON,DoorItems.DioriteDoor,"diorite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(EmeraldDoor = new CustomDoor(Material.WOOD,DoorItems.EmeraldDoor,"emerald_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(EndDoor = new CustomDoor(Material.IRON,DoorItems.EndDoor,"end_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(GlassDoor = new CustomDoor(Material.WOOD,DoorItems.GlassDoor,"glass_door_block",5f,10F,"pickaxe",0, SoundType.GLASS));
		ForgeRegistries.BLOCKS.register(GlowingObsidianDoor = new CustomDoor(Material.IRON,DoorItems.GlowingObsidianDoor,"glowing_obsidian_door_block",5f,10F,"pickaxe",3, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(GoldDoor = new CustomDoor(Material.IRON,DoorItems.GoldDoor,"gold_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(GraniteDoor = new CustomDoor(Material.IRON,DoorItems.GraniteDoor,"granite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(MossStoneDoor = new CustomDoor(Material.IRON,DoorItems.MossStoneDoor,"moss_stone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(ObsidianDoor = new CustomDoor(Material.IRON,DoorItems.ObsidianDoor,"obsidian_door_block",5f,10F,"pickaxe",3, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(RedstoneDoor);
		ForgeRegistries.BLOCKS.register(RubyDoor = new CustomDoor(Material.WOOD,DoorItems.RubyDoor,"ruby_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(RustyDoor = new CustomDoor(Material.IRON,DoorItems.RustyDoor,"rusty_door_block",5f,10F,"pickaxe",1, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(SilverDoor = new CustomDoor(Material.IRON,DoorItems.SilverDoor,"silver_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(StoneDoor = new CustomDoor(Material.IRON,DoorItems.StoneDoor,"stone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
	}
}
