package net.paradisemod.building;

import net.paradisemod.base.Utils;
import net.paradisemod.building.items.CustomDoorItem;
import net.paradisemod.building.items.ItemRedstoneDoor;
import net.minecraft.item.Item;

public class DoorItems {
	// doors
	public static CustomDoorItem AndesiteDoor;
	public static CustomDoorItem BedrockDoor;
	public static CustomDoorItem CactusDoor;
	public static CustomDoorItem CobblestoneDoor;
	public static CustomDoorItem DiamondDoor;
	public static CustomDoorItem DioriteDoor;
	public static CustomDoorItem EmeraldDoor;
	public static CustomDoorItem EndDoor;
	public static CustomDoorItem GlassDoor;
	public static CustomDoorItem GlowingObsidianDoor;
	public static CustomDoorItem GoldDoor;
	public static CustomDoorItem GraniteDoor;
	public static CustomDoorItem MossStoneDoor;
	public static CustomDoorItem ObsidianDoor;
	public static Item RedstoneDoor = new ItemRedstoneDoor();
	public static CustomDoorItem RubyDoor;
	public static CustomDoorItem RustyDoor;
	public static CustomDoorItem SilverDoor;
	public static CustomDoorItem StoneDoor;
	
	public static void init() {
		Utils.regItem(AndesiteDoor = new CustomDoorItem(Doors.AndesiteDoor,"andesite_door","ItemAndesiteDoor"));
		Utils.regItem(BedrockDoor = new CustomDoorItem(Doors.BedrockDoor,"bedrock_door","ItemBedrockDoor"));
		Utils.regItem(CactusDoor = new CustomDoorItem(Doors.CactusDoor,"cactus_door","ItemCactusDoor"));
		Utils.regItem(CobblestoneDoor = new CustomDoorItem(Doors.CobblestoneDoor,"cobblestone_door","ItemCobblestoneDoor"));
		Utils.regItem(DiamondDoor = new CustomDoorItem(Doors.DiamondDoor,"diamond_door","ItemDiamondDoor"));
		Utils.regItem(DioriteDoor = new CustomDoorItem(Doors.DioriteDoor,"diorite_door","ItemDioriteDoor"));
		Utils.regItem(EmeraldDoor = new CustomDoorItem(Doors.EmeraldDoor,"emerald_door","ItemEmeraldDoor"));
		Utils.regItem(EndDoor = new CustomDoorItem(Doors.EndDoor,"end_door","ItemEndDoor"));
		Utils.regItem(GlassDoor = new CustomDoorItem(Doors.GlassDoor,"glass_door","ItemGlassDoor"));
		Utils.regItem(GlowingObsidianDoor = new CustomDoorItem(Doors.GlowingObsidianDoor,"glowing_obsidian_door","ItemGlowingObsidianDoor"));
		Utils.regItem(GoldDoor = new CustomDoorItem(Doors.GoldDoor,"gold_door","ItemGoldDoor"));
		Utils.regItem(GraniteDoor = new CustomDoorItem(Doors.GraniteDoor,"granite_door","ItemGraniteDoor"));
		Utils.regItem(MossStoneDoor = new CustomDoorItem(Doors.MossStoneDoor,"moss_stone_door","ItemMossStoneDoor"));
		Utils.regItem(ObsidianDoor = new CustomDoorItem(Doors.ObsidianDoor,"obsidian_door","ItemObsidianDoor"));
		Utils.regItem(RedstoneDoor);
		Utils.regItem(RubyDoor = new CustomDoorItem(Doors.RubyDoor,"ruby_door","ItemRubyDoor"));
		Utils.regItem(RustyDoor = new CustomDoorItem(Doors.RustyDoor,"rusty_door","ItemRustyDoor"));
		Utils.regItem(SilverDoor = new CustomDoorItem(Doors.SilverDoor,"silver_door","ItemSilverDoor"));
		Utils.regItem(StoneDoor = new CustomDoorItem(Doors.StoneDoor,"stone_door","ItemStoneDoor"));
	}
	
	public static void regRenders() {
		Utils.regRender(RustyDoor);
		Utils.regRender(GlowingObsidianDoor);
		Utils.regRender(EndDoor);
		Utils.regRender(CactusDoor);
		Utils.regRender(SilverDoor);
		Utils.regRender(GlassDoor);
		Utils.regRender(GoldDoor);
		Utils.regRender(DiamondDoor);
		Utils.regRender(EmeraldDoor);
		Utils.regRender(AndesiteDoor);
		Utils.regRender(DioriteDoor);
		Utils.regRender(GraniteDoor);
		Utils.regRender(StoneDoor);
		Utils.regRender(CobblestoneDoor);
		Utils.regRender(MossStoneDoor);
		Utils.regRender(ObsidianDoor);
		Utils.regRender(BedrockDoor);
		Utils.regRender(RubyDoor);
		Utils.regRender(RedstoneDoor);
	}
}
