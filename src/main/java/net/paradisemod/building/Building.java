package net.paradisemod.building;

import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.base.blocks.BasicBlock;
import net.paradisemod.world.blocks.RegenerationStone;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import org.apache.logging.log4j.Level;

public class Building {
    // deep void blocks
    public static Block PolishedVoidStone = new Block(Material.ROCK);
    public static Block RegenerationStone = new RegenerationStone();
    public static Block VoidBricks = new Block(Material.ROCK);
    public static Block VoidStone = new Block(Material.ROCK);

    // other building blocks
    public static BasicBlock glowingObsidian = new BasicBlock(Material.ROCK, SoundType.STONE,true);
    public static Block PolishedEndStone = new Block(Material.ROCK);
    public static BasicBlock CompressedCactus = new BasicBlock(Material.WOOD, SoundType.WOOD, false);

    public static void init() {
        Doors.init();
        DoorItems.init();
        Fences.init();
        Gates.init();
        Slabs.init();
        Stairs.init();
        Trapdoors.init();
        Walls.init();

        Utils.regBlock(PolishedVoidStone.setUnlocalizedName("PolishedVoidStone").setRegistryName("polished_void_stone").setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        Utils.regBlock(RegenerationStone);
        Utils.regBlock(VoidBricks.setUnlocalizedName("VoidBricks").setRegistryName("void_bricks").setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        Utils.regBlock(VoidStone.setUnlocalizedName("VoidStone").setRegistryName("void_stone").setHardness(5F).setResistance(15F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));


        Utils.regBlock(PolishedEndStone.setUnlocalizedName("PolishedEndStone").setRegistryName("polished_end_stone").setHardness(5F).setResistance(15F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        glowingObsidian.setHarvestLevel("pickaxe",3);
        Utils.regBlock(CompressedCactus.setUnlocalizedName("CompressedCactus").setRegistryName("compressed_cactus").setHardness(2.5F).setResistance(5F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        Utils.regBlock(glowingObsidian.setUnlocalizedName("glowingObsidian").setRegistryName("glowing_obsidian").setHardness(51F).setResistance(2000F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setLightLevel(.46666667F));

        ParadiseMod.LOG.log(Level.INFO,"Loaded building module");
    }

    public static void regRenders() {
        DoorItems.regRenders();
        Fences.regRenders();
        Gates.regRenders();
        Slabs.regRenders();
        Stairs.regRenders();
        Trapdoors.regRenders();
        Walls.regRenders();
        Utils.regRender(PolishedVoidStone);
        Utils.regRender(RegenerationStone);
        Utils.regRender(VoidBricks);
        Utils.regRender(VoidStone);
        Utils.regRender(CompressedCactus);
        Utils.regRender(glowingObsidian);
        Utils.regRender(PolishedEndStone);
    }
}
