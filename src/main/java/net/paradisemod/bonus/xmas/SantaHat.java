package net.paradisemod.bonus.xmas;

import net.paradisemod.bonus.Bonus;
import net.paradisemod.base.ModConfig;
import net.minecraft.block.BlockPumpkin;
import net.minecraft.block.SoundType;

public class SantaHat extends BlockPumpkin {
	public SantaHat() {
		setUnlocalizedName("SantaHat");
		setRegistryName("santa_hat");
		setHardness(.01F);
		setResistance(.01F);
		if (!ModConfig.HideXmasFeatures)
			setCreativeTab(Bonus.xmas_tab);
		setSoundType(SoundType.CLOTH);
	}
}