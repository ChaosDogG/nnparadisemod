package net.paradisemod.bonus;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.BannerPattern;
import net.minecraftforge.common.util.EnumHelper;

public class zombieCharge {
    public static String name = "zombie_charge";
    public static String id = "zs";

    public static void addBannerPattern() {
        EnumHelper.addEnum(BannerPattern.class, name.toUpperCase(), new Class[]{String.class, String.class, ItemStack.class},new Object[] {name, id, new ItemStack(Items.SKULL,1,2)});
    }
}
